import os
from pathlib import Path
import time
import sys
import json
from shutil import move
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from infi.systray import SysTrayIcon
import logging

myfolder= 'E:/Users/folder'
distFolder = myfolder
running = True

def getFileName(fileName, distFolder, distSubFolder):
    newName = fileName
    folder = distFolder+'/'+distSubFolder+'/'
    folderExists = os.path.exists(folder)
    if not folderExists:
        os.mkdir(folder)
    fileExists = os.path.isfile(folder+fileName)
    i=0
    while fileExists:
        splitName = fileName.split('.')
        n=len(splitName)
        newName = '.'.join(splitName[0:n-1])
        newName+=' (' +str(i)+').'+splitName[n-1]
        fileExists = os.path.isfile(folder+newName)
        i+=1
    return newName

def organize(myfolder, distFolder):
    for filename in os.listdir(myfolder):
        src = myfolder+'/'+filename
        if(os.path.isfile(src)):
            currentDirectory = os.path.dirname(os.path.realpath(__file__))
            jsonFilePath = os.path.join(currentDirectory, 'distinationFolders.json')

            with open(jsonFilePath) as json_File:
                        distinationFolders = json.load(json_File)
            extension = 'noname'
            logging.info('Processing '+filename)
            try:
                extension = (os.path.splitext(src)[1])
                extension = extension.lower()
                if (extension!='') and (extension in distinationFolders):
                        if (distinationFolders[extension]!=''):#is it an ignored extension eg: partially downloaded files
                            newName = getFileName(filename, distFolder, distinationFolders[extension])
                            move(src, distFolder+'/'+distinationFolders[extension]+'/'+newName)
                        else:
                            logging.info(filename+ ' ignored')
                        
                else:
                    extension = 'noname'
                    newName = getFileName(filename, distFolder, distinationFolders[extension])
                    move(src, distFolder+'/'+distinationFolders[extension]+'/'+newName)
                logging.info(filename+ ' moved to '+ distinationFolders[extension]+ ' as '+ newName)
            except Exception as e:
                extension = 'noname'
                logging.warning('exception ')

class mHandler (FileSystemEventHandler):
    def on_modified(self, event):
        if not event.is_directory :
            logging.warning('Event detected')
            organize(myfolder, distFolder)
            logging.warning('Folder monitoring...')
def openLog(systray):
    os.startfile(logFile)
    logging.info("Log File Opened")

def on_quit_callback(systray):
    logging.warning('Exit')
    running = False
    obs.stop()
    obs.join()
    os._exit(0)

def checkFolder(systray):
    logging.info('checkFolder')
    organize(myfolder, distFolder)

if __name__=='__main__':
    
    typeFolders=['Applications','Divers','Video','3DObjects','Programming',
                'Systeme','Database','Audio','Disk','Inconnu','Documents',
                'Compressed','Images']
    currentDirectory = os.path.dirname(os.path.realpath(__file__))
    logFile = os.path.join(currentDirectory,'app.log')
    logging.basicConfig(filename=logFile, filemode='a', format='%(asctime)s - %(message)s', level=logging.INFO)
    for d in typeFolders:
        typeFolder=os.path.join(distFolder,d)

        if not os.path.exists(typeFolder):
            os.mkdir(typeFolder)
            logging.info(typeFolder+ ' Created')
            

    organize(myfolder, distFolder)

    menuOptions = (("Open Log", None, openLog),("Check folder", None, checkFolder),)
    systray = SysTrayIcon(os.path.join(currentDirectory,"qb_icon.ico"), "AutoOrganizer", menuOptions, on_quit=on_quit_callback)
    logging.info('Process ID : '+str(os.getpid() ))
    eHandler = mHandler()
    obs = Observer()
    obs.schedule(eHandler, myfolder, recursive=False)
    systray.start()
    obs.start()

    logging.info('Folder monitoring...')
    try:
        while running:
            time.sleep(1)
    except KeyboardInterrupt:
        systray.shutdown()
        obs.stop()
        logging.warning('Exit')

    obs.join()
    os._exit(0)








