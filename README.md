# autoOrganizer

Python script to organize a folder.

This script run on the background to organize all the files present in a folder into different folders by category (i.e *.exe -> applications, *.png -> Images, etc).
## dependencies
* Python 3
* watchdog module
* infi.systray module

## Installation
* install python 3
* open a command prompt
* type ``` pip install watchdog ``` to install watchdog
* type ``` pip install infi.systray ``` to install infi.systray

## Running
srcfolder : is the source folder where all the files to be organized.

distfolder : is the parent folder where all the files will organized. It is possible to set distfolder equal to srcfolder.

To use the script follow these steps:
* Edit the srcfolder and distfolder
* Run the script ```AutoOrganizer.bat ```
* Enjoy


## Contact
qubevr@gmail.com